<?php

use yii\db\Migration;

/**
 * Class m200801_123409_category_path
 */
class m200801_123409_category_path extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'path', 'varchar(255) after translation');
        $this->addColumn('{{%category}}', 'base_url', 'varchar(255) after path');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200801_123409_category_path cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200801_123409_category_path cannot be reverted.\n";

        return false;
    }
    */
}
