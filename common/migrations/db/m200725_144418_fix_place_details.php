<?php

use yii\db\Migration;

/**
 * Class m200725_144418_fix_place_details
 */
class m200725_144418_fix_place_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci';
        }

        if($this->getDb()->getTableSchema('{{%place_details}}')){
            $this->dropTable('{{%place_details}}');
        }
        if(!$this->getDb()->getTableSchema('{{%place_details}}')){
            $this->createTable('{{%place_details}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'place_id' => $this->integer(11)->unsigned()->null(),
                'owner_name' => $this->string(255)->null(),
                'mobile' => $this->string(255)->null(),
                'email' => $this->string(255)->null(),
                'website' => $this->string(255)->null(),
                'national_id' => $this->string(25)->null()->defaultValue(1),
                'owner_nid' => $this->string(25)->null()->defaultValue(1),
                'description' => $this->text()->null(),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200725_144418_fix_place_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200725_144418_fix_place_details cannot be reverted.\n";

        return false;
    }
    */
}
