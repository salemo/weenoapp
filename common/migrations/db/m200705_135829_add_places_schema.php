<?php

use yii\db\Migration;

/**
 * Class m200705_135829_add_places_schema
 */
class m200705_135829_add_places_schema extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci';
        }

        if(!$this->getDb()->getTableSchema('{{%category}}')){
            $this->createTable('{{%category}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'name' => $this->string(255)->unsigned()->null(),
                'translation' => $this->string(255)->null(),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
                'isBlocked' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
        if(!$this->getDb()->getTableSchema('{{%place}}')){
            $this->createTable('{{%place}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'name' => $this->string(255)->unsigned()->null(),
                'translation' => $this->string(255)->null(),
                'address_title' => $this->string(255)->null(),
                'latitude' => $this->float()->null(),
                'longitude' => $this->float()->null()->defaultValue(1),
                'category_id' => $this->integer(11)->null()->defaultValue(1),
                'city_id' => $this->integer(11)->null()->defaultValue(1),
                'country_id' => $this->integer(11)->null()->defaultValue(1),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
                'isBlocked' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
        if(!$this->getDb()->getTableSchema('{{%place_details}}')){
            $this->createTable('{{%place_details}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'place_id' => $this->integer(11)->unsigned()->null(),
                'owner_name' => $this->string(255)->null(),
                'mobile' => $this->string(255)->null(),
                'email' => $this->string(255)->null(),
                'website' => $this->float()->null(),
                'national_id' => $this->float()->null()->defaultValue(1),
                'owner_nid' => $this->integer(11)->null()->defaultValue(1),
                'description' => $this->text()->null(),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
        if(!$this->getDb()->getTableSchema('{{%place_gallery}}')){
            $this->createTable('{{%place_gallery}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'place_id' => $this->integer(11)->unsigned()->null(),
                'path' => $this->string(255)->unsigned()->null(),
                'base_url' => $this->string(255)->null(),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
        if(!$this->getDb()->getTableSchema('{{%city}}')){
            $this->createTable('{{%city}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'name' => $this->string(20)->unsigned()->null(),
                'translation' => $this->string(255)->null(),
                'country_id' => $this->integer(11)->null()->defaultValue(1),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
                'isBlocked' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);
        }
        if(!$this->getDb()->getTableSchema('{{%country}}')){
            $this->createTable('{{%country}}', [
                'id' => $this->primaryKey()->unsigned()->notNull(),
                'name' => $this->string(20)->unsigned()->null(),
                'translation' => $this->string(255)->null(),
                'created_at' => $this->datetime()->null(),
                'updated_at' => $this->datetime()->null(),
                'added_by' => $this->integer(11)->null()->defaultValue(0),
                'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
            ], $tableOptions);

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200705_135829_add_places_schema cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200705_135829_add_places_schema cannot be reverted.\n";

        return false;
    }
    */
}
