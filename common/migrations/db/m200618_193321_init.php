<?php

use yii\db\Migration;

/**
 * Class m200618_193321_init
 */
class m200618_193321_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'phone' => $this->string(20)->unsigned()->null(),
            'accessToken' => $this->string(255)->null(),
            'password' => $this->string(255)->null(),
            'name' => $this->string(255)->null(),
            'email' => $this->string(255)->null(),
            'userStatus' => $this->smallInteger(1)->null()->defaultValue(1),
            'userType' => $this->smallInteger(1)->null()->defaultValue(1),
            'notifications' => $this->smallInteger(1)->null()->defaultValue(1),
            'created_at' => $this->datetime()->null(),
            'updated_at' => $this->datetime()->null(),
            'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
            'isBlocked' => $this->smallInteger(1)->null()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%user_address}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'user_id' => $this->integer(11)->unsigned()->notNull(),
            'longitude' => $this->decimal(20)->notNull(),
            'latitude' => $this->decimal(20)->notNull(),
            'address' => $this->string(1024)->notNull(),
            'flat_no' => $this->string(20)->null()->defaultValue('0'),
            'house_no' => $this->string(20)->null()->defaultValue('0'),
            'apartment' => $this->string(20)->null()->defaultValue('0'),
            'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%user_device}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'user_id' => $this->integer(11)->null(),
            'device_token' => $this->string(255)->null(),
        ], $tableOptions);

        $this->createTable('{{%user_otp}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'userId' => $this->integer(11)->notNull(),
            'otp' => $this->integer(11)->notNull(),
            'status' => $this->integer(11)->notNull(),
            'generatedOn' => $this->string(255)->null(),
            'isDeleted' => $this->integer(11)->null()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%user_profile}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'firstName' => $this->string(255)->null(),
            'middleName' => $this->string(255)->null(),
            'lastName' => $this->string(255)->null(),
            'avatar_path' => $this->string(255)->null(),
            'avatar_base_url' => $this->string(255)->null(),
            'locale' => $this->string(32)->null(),
            'gender' => $this->smallInteger(1)->null(),
        ], $tableOptions);

        $this->createTable('{{%file_storage_item}}', [
            'id' => $this->primaryKey()->notNull(),
            'component' => $this->string(255)->notNull(),
            'base_url' => $this->string(1024)->notNull(),
            'path' => $this->string(1024)->notNull(),
            'type' => $this->string(255)->null(),
            'size' => $this->integer(11)->null(),
            'name' => $this->string(255)->null(),
            'upload_ip' => $this->string(15)->null(),
            'created_at' => $this->integer(11)->null()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%key_storage_item}}', [
            'key' => $this->string(128)->notNull(),
            'value' => $this->text()->notNull(),
            'comment' => $this->text()->null(),
            'updated_at' => $this->integer(11)->null(),
            'created_at' => $this->integer(11)->null(),
        ], $tableOptions);

        $this->createTable('{{%notifications}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'type' => $this->integer(2)->notNull(),
            'title' => $this->string(255)->notNull(),
            'message' => $this->string(1024)->notNull(),
            'user_notified' => $this->integer(2)->null()->defaultValue(0),
            'created_by' => $this->integer(11)->unsigned()->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'isDeleted' => $this->smallInteger(1)->null()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%rbac_auth_assignment}}', [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(11)->null(),
        ], $tableOptions);


        $this->createTable('{{%rbac_auth_item}}', [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer(11)->notNull(),
            'description' => $this->text()->null(),
            'rule_name' => $this->string(64)->null(),
            'data' => $this->text()->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], $tableOptions);

        $this->createTable('{{%rbac_auth_item_child}}', [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
        ], $tableOptions);

        $this->createTable('{{%rbac_auth_rule}}', [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text()->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ], $tableOptions);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200618_193321_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200618_193321_init cannot be reverted.\n";

        return false;
    }
    */
}
