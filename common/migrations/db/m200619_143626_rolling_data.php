<?php

use yii\db\Migration;

/**
 * Class m200619_143626_rolling_data
 */
class m200619_143626_rolling_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $user = new \common\models\User();
        $user->setPassword("123123");
        $user->generateAuthKey();
        $user->name = "Mohammad Salem";
        $user->email = "admin@example.me";
        $user->userStatus = \common\models\User::STATUS_ACTIVE;
        $user->phone = "0788080045";
        $user->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200619_143626_rolling_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200619_143626_rolling_data cannot be reverted.\n";

        return false;
    }
    */
}
