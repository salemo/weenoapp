<?php
namespace common\behaviors;

use Yii;
use yii\web\Controller;

class BehaviorController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
}
