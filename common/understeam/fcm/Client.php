<?php

namespace common\understeam\fcm;

use common\paragraph1\phpFCM\Message;
use common\paragraph1\phpFCM\Notification;

/**
 * Client class dispatches paragraph1\phpFCM entities
 * @author Anatoly Rugalev <anatoly.rugalev@gmail.com>
 */
class Client extends \understeam\fcm\Client
{
    /**
     * Creates Message object
     * @param string[]|string $deviceTokens tokens of recipient devices
     * @return Message
     */
    public function createMessage($deviceTokens = [])
    {
        $message = new Message();
        if (is_string($deviceTokens)) {
            $deviceTokens = [$deviceTokens];
        }
        if (!is_array($deviceTokens)) {
            throw new InvalidParamException("\$deviceTokens must be string or array");
        }
        foreach ($deviceTokens as $token) {
            $message->addRecipient($this->createDevice($token));
        }
        return $message;
    }
    
    /**
     * Creates Notification object
     * @param string $title Notification title
     * @param string $body Notification body text
     * @return Notification
     */
    public function createNotification($title, $body)
    {
        return new Notification($title, $body);
    }
    
    protected function createFcm()
    {
        $client = new \common\paragraph1\phpFCM\Client();
        $client->setApiKey($this->apiKey);
        $client->setProxyApiUrl($this->proxyApiUrl);
        $client->injectHttpClient($this->getHttpClient());
        
        return $client;
    }
}
