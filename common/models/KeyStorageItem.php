<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property integer $key
 * @property integer $value
 */
class KeyStorageItem extends ActiveRecord
{
    const AUTO_PICKUP_TIME = 'auto-pickup-time';
    const EVENT_AUTO_PICKUP_SETTING = 'auto-pickup-setting';
    const VAT = 'vat';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%key_storage_item}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max' => 128],
            [['value', 'comment'], 'safe'],
            [['key'], 'unique'],
            ['value', 'number', 'min' => 0.1, 'max' => 99.99, 'when' => function($model) {
                return (int) $model->key === self::VAT;
			}],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }
    
    public function isAutoPickup()
    {
        return $this->key === self::AUTO_PICKUP_TIME;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if($this->isAutoPickup()) {
            $this->on(self::EVENT_AUTO_PICKUP_SETTING, [new log\OrderLog(), 'autoPickupSetting']);
            $this->trigger(self::EVENT_AUTO_PICKUP_SETTING);
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
}
