<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "place_gallery".
 *
 * @property int $id
 * @property int|null $place_id
 * @property string|null $path
 * @property string|null $base_url
 * @property int|null $isDeleted
 *
 * @property string $url
 */
class PlaceGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id', 'isDeleted'], 'integer'],
            [['path', 'base_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Place ID',
            'path' => 'Path',
            'base_url' => 'Base Url',
            'isDeleted' => 'Is Deleted',
        ];
    }

    public function getUrl(){
        return $this->base_url . $this->path;
    }

}
