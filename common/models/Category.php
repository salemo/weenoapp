<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $translation
 * @property string|null $path
 * @property string|null $base_url
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $added_by
 * @property int|null $isDeleted
 * @property int|null $isBlocked
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['added_by', 'isDeleted', 'isBlocked'], 'integer'],
            [['name', 'translation'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'translation' => 'Translation',
            'path' => 'Category Image',
            'base_url' => 'Translation',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'added_by' => 'Added By',
            'isDeleted' => 'Is Deleted',
            'isBlocked' => 'Is Blocked',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CategoryQuery(get_called_class());
    }

    public function getImage(){
        return $this->base_url . $this->path;
    }
}
