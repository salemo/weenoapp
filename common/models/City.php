<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $translation
 * @property int|null $country_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $added_by
 * @property int|null $isDeleted
 * @property int|null $isBlocked
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'added_by', 'isDeleted', 'isBlocked'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 20],
            [['translation'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'translation' => 'Translation',
            'country_id' => 'Country ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'added_by' => 'Added By',
            'isDeleted' => 'Is Deleted',
            'isBlocked' => 'Is Blocked',
        ];
    }

    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
