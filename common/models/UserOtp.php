<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_otp".
 *
 * @property int $id
 * @property int $userId
 * @property int $otp
 * @property int $status
 * @property string|null $generatedOn
 * @property int|null $isDeleted
 */
class UserOtp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_otp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'otp', 'status'], 'required'],
            [['userId', 'otp', 'status', 'isDeleted'], 'integer'],
            [['generatedOn'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'otp' => 'Otp',
            'status' => 'Status',
            'generatedOn' => 'Generated On',
            'isDeleted' => 'Is Deleted',
        ];
    }
}
