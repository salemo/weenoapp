<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property int $type
 * @property string $title
 * @property string $message
 * @property int|null $user_notified
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $isDeleted
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'title', 'message', 'created_by', 'created_at', 'updated_at'], 'required'],
            [['type', 'user_notified', 'created_by', 'created_at', 'updated_at', 'isDeleted'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'message' => 'Message',
            'user_notified' => 'User Notified',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'isDeleted' => 'Is Deleted',
        ];
    }
}
