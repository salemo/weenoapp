<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "place_details".
 *
 * @property int $id
 * @property int|null $place_id
 * @property string|null $owner_name
 * @property string|null $mobile
 * @property string|null $email
 * @property string|null $website
 * @property string|null $national_id
 * @property string|null $owner_nid
 * @property string|null $description
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $added_by
 * @property int|null $isDeleted
 */
class PlaceDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id', 'added_by', 'isDeleted'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['owner_name', 'mobile', 'email', 'website'], 'string', 'max' => 255],
            [['national_id', 'owner_nid'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Place ID',
            'owner_name' => 'Owner Name',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'website' => 'Website',
            'national_id' => 'National ID',
            'owner_nid' => 'Owner Nid',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'added_by' => 'Added By',
            'isDeleted' => 'Is Deleted',
        ];
    }
}
