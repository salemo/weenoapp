<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_address".
 *
 * @property int $id
 * @property int $user_id
 * @property float $longitude
 * @property float $latitude
 * @property string $address
 * @property string|null $flat_no
 * @property string|null $house_no
 * @property string|null $apartment
 * @property int|null $isDeleted
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'longitude', 'latitude', 'address'], 'required'],
            [['user_id', 'isDeleted'], 'integer'],
            [['longitude', 'latitude'], 'number'],
            [['address'], 'string', 'max' => 1024],
            [['flat_no', 'house_no', 'apartment'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'address' => 'Address',
            'flat_no' => 'Flat No',
            'house_no' => 'House No',
            'apartment' => 'Apartment',
            'isDeleted' => 'Is Deleted',
        ];
    }
}
