<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_device".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $device_token
 */
class UserDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['device_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'device_token' => 'Device Token',
        ];
    }
}
