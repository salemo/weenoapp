<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $translation
 * @property string|null $address_title
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int|null $category_id
 * @property int|null $city_id
 * @property int|null $country_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $added_by
 * @property int|null $isDeleted
 * @property int|null $isBlocked
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude'], 'number'],
            [['category_id', 'city_id', 'country_id', 'added_by', 'isDeleted', 'isBlocked'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'translation', 'address_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'translation' => 'Translation',
            'address_title' => 'Address Title',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'category_id' => 'Category ID',
            'city_id' => 'City ID',
            'country_id' => 'Country ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'added_by' => 'Added By',
            'isDeleted' => 'Is Deleted',
            'isBlocked' => 'Is Blocked',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\PlaceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PlaceQuery(get_called_class());
    }

    public function getDetails(){
        return $this->hasOne(PlaceDetails::className(), ['place_id' => 'id']);
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getCity(){
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getImages(){
        return $this->hasMany(PlaceGallery::className(), ['place_id' => 'id']);
    }
}
