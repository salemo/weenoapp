<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\UserDevice]].
 *
 * @see \common\models\UserDevice
 */
class UserDeviceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\UserDevice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\UserDevice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
