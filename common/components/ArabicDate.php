<?php
namespace common\components;

class ArabicDate
{
    public function arabicDate($date)
    {
        $months = [
            "Jan" => "1",
            "Feb" => "2",
            "Mar" => "3",
            "Apr" => "4",
            "May" => "5",
            "Jun" => "6",
            "Jul" => "7",
            "Aug" => "8",
            "Sep" => "9",
            "Oct" => "10",
            "Nov" => "11",
            "Dec" => "12"
        ];

        $en_month = date("M", strtotime($date));
        $en_day   = date("d", strtotime($date));
        $en_year  = date("Y", strtotime($date));
        $en_Hour  = date("h", strtotime($date));
        $en_min  = date("i", strtotime($date));

        foreach ($months as $en => $ar) {
            if ($en == $en_month) {
                $ar_month = $ar;
            }
        }

        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $current_date =  $en_day . '-' . $ar_month . '-' . $en_year . ' ' . $en_Hour . ':' . $en_min;

        $arabic_date = str_replace($standard, $eastern_arabic_symbols, $current_date);
        return $arabic_date;
    }

    public function convertNumbers($Time){
        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $eastern_arabic_symbols = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $arabic_date = str_replace($eastern_arabic_symbols, $standard, $Time);

        return $arabic_date;
    }
}

