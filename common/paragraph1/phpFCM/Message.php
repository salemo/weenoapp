<?php
namespace common\paragraph1\phpFCM;

/**
 * @author palbertini
 */
class Message extends \paragraph1\phpFCM\Message
{
	private $contentAvailable = true;

    public function setContentAvailable($contentAvailable)
    {
        $this->contentAvailable = $contentAvailable;
        return $this;
    }

    public function jsonSerialize()
    {
        $jsonData = parent::jsonSerialize();
        if ($this->contentAvailable) {
            $jsonData['content_available'] = (bool)$this->contentAvailable;
        }

        return $jsonData;
    }
}