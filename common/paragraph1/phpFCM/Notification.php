<?php

namespace common\paragraph1\phpFCM;

/**
 * @link https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support
 */
class Notification extends \paragraph1\phpFCM\Notification
{
    public  $sound;
    public  $badge;
    private $click_action;
    /**
     * android/ios: can be default or a filename of a sound resource bundled in the app.
     * @see https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support
     *
     * @param string $sound a sounds filename
     *
     * @return \paragraph1\phpFCM\Notification
     */
    public function setSound($sound)
    {
        $this->sound = $sound;
        return $this;
    }

    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }
    
    public function jsonSerialize()
    {
		$jsonData = parent::jsonSerialize();
		$jsonData['sound'] = $this->sound;
		$jsonData['badge'] = $this->badge;
		$jsonData['click_action'] = $this->click_action;

		return $jsonData;
	}

    public function setClickAction($click_action)
    {
        $this->click_action = $click_action;
        return $this;
    }

}