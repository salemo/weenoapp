<?php
namespace common\paragraph1\phpFCM;

use Yii;
/**
 * @author palbertini
 */
class Client extends \paragraph1\phpFCM\Client
{
    public function injectHttpClient(\GuzzleHttp\ClientInterface $client)
    {
        $this->guzzleClient = $client;
    }
    
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }
    
    private function getApiUrl()
    {
        return isset($this->proxyApiUrl) ? $this->proxyApiUrl : self::DEFAULT_API_URL;
    }
   /**
     * sends your notification to the google servers and returns a guzzle repsonse object
     * containing their answer.
     *
     * @param Message $message
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\RequestException
     */
    public function send(\paragraph1\phpFCM\Message $message)
    {
		Yii::info('Request: ' . json_encode($message),  'firebase');
        $res =  $this->guzzleClient->post(
            $this->getApiUrl(),
            [
                'headers' => [
                    'Authorization' => sprintf('key=%s', $this->apiKey),
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($message)
            ]
        );

		Yii::info('Response: ' . $res->getBody(), 'firebase');

		return $res;
    }
}