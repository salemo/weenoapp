<?php

namespace common\base;

class Model extends \yii\base\Model
{
    private $_errorCodes;
    /**
     * Adds a new error to the specified attribute.
     * @param string $attribute attribute name
     * @param string $error new error message
     */
    public function addErrorCode($attribute, $error = '')
    {
        $this->_errorCodes[$attribute][] = $error;
    }

    /**
     * Returns a value indicating whether there is any validation error.
     * @param string|null $attribute attribute name. Use null to check all attributes.
     * @return boolean whether there is any error.
     */
    public function hasErrorCodes($attribute = null)
    {
        return $attribute === null ? !empty($this->_errorCodes) : isset($this->_errorCodes[$attribute]);
    }

    public function getErrorCodes($attribute = null)
    {
        if ($attribute === null) {
            return $this->_errorCodes === null ? [] : $this->_errorCodes;
        } else {
            return isset($this->_errorCodes[$attribute]) ? $this->_errorCodes[$attribute] : [];
        }
    }

    public function getFirstErrorCodes()
    {
        if (empty($this->_errorCodes)) {
            return [];
        } else {
            $errors = [];
            foreach ($this->_errorCodes as $name => $es) {
                if (!empty($es)) {
                    $errors[$name] = reset($es);
                }
            }

            return $errors;
        }
    }

    public function getFirstErrorCode($attribute)
    {
        return isset($this->_errorCodes[$attribute]) ? reset($this->_errorCodes[$attribute]) : null;
    }

}