<?php
/**
 * Require core files
 */
require_once(__DIR__ . '/../helpers.php');

/**
 * Setting path aliases
 */
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@rest', dirname(dirname(__DIR__)) . '/rest');
Yii::setAlias('@storage', dirname(dirname(__DIR__)) . '/storage');

/**
 * Setting url aliases
 */
Yii::setAlias('@frontendUrl', env("FRONTEND_URL"));
Yii::setAlias('@backendUrl',  env("BACKEND_URL"));
Yii::setAlias('@storageUrl',  env("STORAGE_URL"));
Yii::setAlias('@restUrl',      env("API_URL"));