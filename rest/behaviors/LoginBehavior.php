<?php

namespace rest\behaviors;

use common\models\User;
use yii\base\Behavior;
use Yii;

class LoginBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            User::EVENT_AFTER_LOGIN => 'afterLogin'
        ];
    }

    /**
     * @param $event \yii\web\UserEvent
     */
    public function afterLogin($event)
    {
        $user = User::findOne(['id' => $event->identity->getId()]);
        if (empty($user->accessToken)) {
            $user->resetAccessToken();
        }
    }
}
