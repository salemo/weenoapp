<?php
$path = strtok($_SERVER['REQUEST_URI'], '?');

if($path !== '/debug') {
    list($getVerson, $getPath) = explode('/' ,ltrim($_SERVER['REQUEST_URI'], '/'));
    $version = $getVerson;
} else {
    $version = 'v2';
}

return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => [
            'v1/post',
            'v1/get',
            'v1/comment',
            'v2/post',
            'v2/get',
            'v2/comment',
        ]
    ],
    /* Login */
    'OPTIONS v<versionId:\d+>/login' => "$version/auth/login",
    'POST v<versionId:\d+>/login' => "$version/auth/login",

    /* Logout */
    'OPTIONS v<versionId:\d+>/logout' => "$version/auth/logout",
    'POST v<versionId:\d+>/logout' => "$version/auth/logout",

    /* Register */
    'OPTIONS v<versionId:\d+>/register' => "$version/auth/register",
    'POST v<versionId:\d+>/register' => "$version/auth/register",


    /* Categories */
    'OPTIONS v<versionId:\d+>/categories' => "$version/places/categories",
    'GET v<versionId:\d+>/categories' => "$version/places/categories",

    /* Places */
    'OPTIONS v<versionId:\d+>/places/<categoryId:\d+>' => "$version/places/places-by-category",
    'GET v<versionId:\d+>/places/<categoryId:\d+>' => "$version/places/places-by-category",

    /* Places Search */
    'OPTIONS v<versionId:\d+>/places' => "$version/search/index",
    'GET v<versionId:\d+>/places' => "$version/search/index",

    /* Places Search */
    'OPTIONS v<versionId:\d+>/search' => "$version/search/search",
    'GET v<versionId:\d+>/search' => "$version/search/search",

    
];
