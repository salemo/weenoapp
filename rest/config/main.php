<?php

use common\models\User;

$params = array_merge(
    require(__DIR__ . '/params.php')
);
$rules = require(__DIR__ . '/urlManagerRules.php');

$config = [
    'id' => 'rest',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'rest\versions\v1\RestModule'
        ],
        'v2' => [
            'class' => 'rest\versions\v2\RestModule'
        ]

    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'as afterLogin' => 'rest\behaviors\LoginBehavior',
            'on afterLogout' => function ($event) {
                $token = $event->identity->access_token;
                return User::removeIdentityByAccessToken($token);
            }
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            'as beforeSend' => 'rest\behaviors\ResponseBehavior',
        ],
        'userAppVersion' => function () {
            $path = strtok($_SERVER['REQUEST_URI'], '?');
            if ($path !== '/debug') {
                list($getVerson, $getPath) = explode('/', ltrim($_SERVER['REQUEST_URI'], '/'));
                $version = $getVerson;
            }
            return $version ?? null;
        },
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'on beforeAction' => function () {

            }
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => $rules,
        ],
        'fcm' => [
            'class' => 'common\understeam\fcm\Client',
            'apiKey' => env('FCM_SERVER_KEY'),
        ],
        'api_language' => function () {
            $headers = Yii::$app->request->headers;
            $language = $headers['language'];
            return strtolower($language ?? 'en');
        },

    ],
    'params' => $params,
];
if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
        'historySize' => 1500
    ];
}

return $config;
