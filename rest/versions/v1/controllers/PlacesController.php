<?php

namespace rest\versions\v1\controllers;

use common\models\Category;
use common\models\Place;
use common\models\PlaceGallery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class PlacesController extends Controller
{


    public function actionIndex()
    {
        $Places = Place::find();
        $request = Yii::$app->request;
        $queryParams = $request->getQueryParams();

        if(isset($queryParams['catId'])){
            $Places->andWhere(['category_id' => $queryParams['catId']]);
        }

        if(isset($queryParams['cityId'])){
            $Places->andWhere(['city_id' => $queryParams['cityId']]);
        }

        if(isset($queryParams['placeName'])){
            $Places->andWhere(['LIKE', 'name', $queryParams['placeName']]);
        }


        return $this->placesResponse($Places);

    }

    public function actionPlaceDetails()
    {

    }

    public function actionPlacesByCategory($categoryId)
    {
        $Places = Place::find()->where(['category_id' => $categoryId])->all();
        return $this->placesResponse($Places);
    }

    public function placesResponse($Places){
        return ArrayHelper::toArray($Places, [
            Place::className() => [
                'id' => function ($Place) {
                    return $Place->id ?? null;
                },
                'name' => function ($Place) {
                    return $Place->name ?? null;
                },
                'translation' => function ($Place) {
                    return $Place->translation ?? null;
                },
                'address_title' => function ($Place) {
                    return $Place->address_title ?? null;
                },
                'latitude' => function ($Place) {
                    return $Place->latitude ?? null;
                },
                'longitude' => function ($Place) {
                    return $Place->longitude ?? null;
                },
                'category_id' => function ($Place) {
                    return $Place->category->name ?? null;
                },
                'city_id' => function ($Place) {
                    return $Place->city_id ?? null;
                },
                'images' => function ($Place) {
                    return ArrayHelper::toArray($Place->images, [
                        PlaceGallery::className() => [
                            'url' => function ($Place) {
                                return $Place->url ?? null;
                            },
                        ]
                    ]);
                },
            ]
        ]);
    }

    public function actionCategories()
    {

        $categories = Category::find()->all();
        return ArrayHelper::toArray($categories, [
            Category::className() => [
                'id' => function ($category) {
                    return $category->id ?? null;
                },
                'name' => function ($category) {
                    return $category->name ?? null;
                },
                'translation' => function ($category) {
                    return $category->translation ?? null;
                },
                'image' => function ($category) {
                    return $category->image ?? null;
                },
            ]
        ]);
    }

}