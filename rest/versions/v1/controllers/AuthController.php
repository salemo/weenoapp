<?php

namespace rest\versions\v1\controllers;

use rest\models\LoginForm;
use common\models\User;
use common\models\UserDevice;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class AuthController extends Controller{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionLogin(){
        $model = new LoginForm();
        $params = Yii::$app->getRequest()->getBodyParams();
        if(!isset($params['username']) || empty($params['username'])){
            $error = Yii::$app->params['ERROR']['BLANK_EMAIL'];
            Yii::$app->response->statusCode	 = 422;
            $response['error']['code']		 = key($error) ?? null;
            $response['error']['message']	 = current($error) ?? null;
            return $response;
        }
        if(!isset($params['password']) || empty($params['password'])){
            $error = Yii::$app->params['ERROR']['BLANK_PASSWORD'];
            Yii::$app->response->statusCode	 = 422;
            $response['error']['code']		 = key($error) ?? null;
            $response['error']['message']	 = current($error) ?? null;
            return $response;
        }
        if(!isset($params['deviceToken']) || empty($params['deviceToken'])){
            $error = Yii::$app->params['ERROR']['DEVICE_TOKEN_BLANK'];
            Yii::$app->response->statusCode	 = 422;
            $response['error']['code']		 = key($error) ?? null;
            $response['error']['message']	 = current($error) ?? null;
            return $response;
        }

        if ($model->load($params, '') && $model->login()) {
            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
            $userDevice = new UserDevice();
            $userDevice->user_id = $user->id;
            $userDevice->device_token = $params['deviceToken'];
            $userDevice->save(false);

            Yii::$app->response->statusCode	 = 200;
            return ArrayHelper::toArray($user, [
                User::className() => [
                    'userID' => function ($user) {
                        return $user->id ?? null;
                    },
                    'accessToken' => function ($user) {
                        return $user->accessToken ?? null;
                    },
                    'name' => function ($user) {
                        return $user->name ?? null;
                    },
                    'phone' => function ($user) {
                        return $user->phone ?? null;
                    },
                    'email' => function ($user) {
                        return $user->email ?? null;
                    },
                ]
            ]);
        } else {
            Yii::$app->response->statusCode	 = 401;
            $response['error']['code']		 = current($model->getFirstErrorCodes()) ?? null;
            $response['error']['message']	 = current($model->getFirstErrors()) ?? null;
            return $response;
        }
    }

    public function actionRegister(){
        $params = Yii::$app->request->getBodyParams();
        if (!isset($params['mobile']) || empty($params['mobile'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['BLANK_PHONE']);
        }
        if (!isset($params['email']) || empty($params['email'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['BLANK_EMAIL']);
        }
        if (!isset($params['password']) || empty($params['password'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['BLANK_PASSWORD']);
        }
        if (!isset($params['confirmPassword']) || empty($params['confirmPassword'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['BLANK_PASSWORD']);
        }
        if (!(md5($params['password']) === md5($params['confirmPassword']))) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['MISS_MATCHED_PASSWORD']);
        }
        if (!isset($params['fullName']) || empty($params['fullName'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['BLANK_USERNAME']['FIRST']);
        }

        if (!isset($params['deviceToken']) || empty($params['deviceToken'])) {
            Yii::$app->response->statusCode = 422;
            return $this->formatError(Yii::$app->params['ERROR']['DEVICE_TOKEN_BLANK']);
        }
        $user = new User();
        $user->phone = $params['mobile'];
        $user->password = md5($params['password']);
        $user->name = $params['fullName'];
        $user->email = $params['email'];
        $user->userStatus = User::STATUS_ACTIVE;
        $user->notifications = 1;
        $user->userType = 1;
        $user->isDeleted = 0;

        if ($user->validate()) {
            if ($user->save(false)) {
                $userDevice = new UserDevice();
                $userDevice->user_id = $user->id;
                $userDevice->device_token = $params['deviceToken'];
                $userDevice->save();
                if (empty($user->access_token)) {
                    $user->resetAccessToken();
                }

                Yii::$app->response->statusCode = 200;
                return ArrayHelper::toArray($user, [
                    User::className() => [
                        'userID' => function ($user) {
                            return $user->id ?? null;
                        },
                        'accessToken' => function ($user) {
                            return $user->accessToken ?? null;
                        },
                        'name' => function ($user) {
                            return $user->name ?? null;
                        },
                        'phone' => function ($user) {
                            return $user->phone ?? null;
                        },
                        'email' => function ($user) {
                            return $user->email ?? null;
                        },
                    ]
                ]);
            } else {
                Yii::$app->response->statusCode = 422;
                $response['error']['code'] = 1032;
                $response['error']['message'] = current($user->getFirstErrors()) ?? null;
                return $response;
            }
        } else {
            Yii::$app->response->statusCode = 422;
            $response['error']['code'] = 1033;
            $response['error']['message'] = current($user->getFirstErrors()) ?? null;
            return $response;
        }
    }

    public function actionLogout(){

    }

    public function actionResetPassword(){

    }

    public function actionForgotPassword(){
        Yii::$app->user->logout();
        Yii::$app->response->statusCode	 = 200;
        return [];
    }
}