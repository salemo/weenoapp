<?php
namespace rest\models;

use common\models\User;
use common\base\Model;
use common\models\UserDevice;
use common\models\UserProfile;
use Yii;

/**
 * Registration form
 */
class RegistrationForm extends Model
{
    /**
     * @var
     */
    public $phone;

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $deviceToken;

    /**
     * @var
     */
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'name'], 'validateAttributes'],
            ['phone', 'validatePhone'],
            ['phone', 'validatePattern'],
            [['email', 'deviceToken'],  'string', 'max' => 500],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Password',
            'name' => 'Name',
            'phone' => 'Phone',
            'deviceTokem' => 'Device Token',
        ];
    }

    public static function register($phone)
    {
        $user = new User();
        $user->phone = $phone;
        $user->userStatus = User::STATUS_PENDING;
        $user->isDeleted = 0;
        $user->isBlocked = 0;
        $user->register_date = date('Y-m-d H:i:s');
        $user->save(false);

        $authManager = Yii::$app->authManager;
        $role =  $authManager->getRole(User::ROLE_CUSTOMER);
        $authManager->assign($role,$user->id);

        return $user;
    }


    public function setPassword()
    {
        return md5($this->password);
    }
  
    public function validatePhone($attribute)
    {
        $user = User::find()
            ->notDeleted()
            ->andWhere(['phone' => $this->phone])
            ->one();
        if (!empty($user)) {
            $this->addError($attribute, 'This phone has already registered.');
            $this->addErrorCode($attribute, RestErrorCode::ALREADY_REGISTERED_PHONE);
        }
    }

    public function validatePattern($attribute)
    {
        if (!preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $this->phone)) {
            $this->addError($attribute, 'Phone pattern mismatch.');
            $this->addErrorCode($attribute, RestErrorCode::PHONE_PATTERN_MISMATCH);
        }
    }


    public function validatePasswordLength($attribute)
    {
        if (strlen($this->password) < 6) {
            $this->addError($attribute, 'Password must be at least 6 characters');
            $this->addErrorCode($attribute, RestErrorCode::INCORRECT_PASSWORD_LIMIT);
        }
    }

    public function validateAttributes($attribute)
    {
        if(is_null($this->phone)){
            $this->addError($attribute, 'Phone is required and cannot be empty');
            $this->addErrorCode($attribute, RestErrorCode::PHONE_REQUIRED);
        }

        if(is_null($this->name)){
            $this->addError($attribute, 'Name is required and cannot be empty');
            $this->addErrorCode($attribute, RestErrorCode::NAME_REQUIRED);
        }
    }
}
