<?php

namespace rest\models;

use common\models\User;
use common\models\UserOtp;
use Yii;
use yii\base\Model;
use yii\validators\EmailValidator;

/**
 * Login form
 */
class LoginForm extends \common\base\Model
{
    public $username;
    public $password;
    public $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username'], 'required'],
            ['password', 'validatePassword'],
            ['username', 'email'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }

        return false;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $user = User::findByUsername($this->username);
            if (!empty($user)) {
                $this->_user = $user;
            }
        }

        return $this->_user;
    }

    public function validatePattern($attribute)
    {
        $validator = new EmailValidator();


        if (!$validator->validate($attribute, $error)) {
            $this->addError($attribute, $error);
            $this->addErrorCode($attribute, RestErrorCode::INCORRECT_EMAIL);
        }
    }
}
