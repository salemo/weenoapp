<?php

namespace rest\models;

use common\models\User;
use common\base\Model;
use common\models\UserDevice;
use common\models\UserProfile;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Update User form
 */
class UpdateUserForm extends Model
{
    /**
     * @var
     */

    public $user_id;

    /**
     * @var
     */

    public $phone;

    /**
     * @var
     */

    public $password;

    /**
     * @var
     */
    public $name;
    public $firstName;
    public $lastName;
    /**
     * @var
     */

    public $deviceToken;

    /**
     * @var
     */

    public $email;

    /**
     * @var
     */

    public $notifications;

    /**
     * @var
     */

    public $path;

    /**
     * @var
     */

    public $baseUrl;

    /**
     * @var
     */

    public $url;

    /**
     * @var
     */

    public $locale;

    /**
     * @var
     */

    public $gender;
    /**
     * @var
     */

    public $country;


    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['password', 'validatePasswordLength'],
            ['phone', 'validatePhone'],
            ['phone', 'validatePattern'],
            ['email', 'validateEmailPattern'],
            [['email', 'password', 'country', 'phone', 'name', 'firstName', 'lastName', 'notifications', 'gender', 'locale', 'path', 'baseUrl', 'deviceToken', 'url'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Password',
            'name' => 'Name',
            'phone' => 'Phone',
            'deviceToken' => 'Device Token',
            'notifications' => 'Notifications',
            'gender' => 'Gender',
            'locale' => 'Locale',
            'path' => 'Path',
            'baseUrl' => 'Base Url',
            'url' => 'Url',
        ];
    }

    public function update()
    {
        $user = User::findOne(['id' => $this->user_id]);
        $profile = UserProfile::findOne(['user_id' => $this->user_id]);
        if (!$profile) {
            $profile = new UserProfile();
        }
        $profile->user_id = $this->user_id;

        if (!is_null($this->phone) && !empty($this->phone)) {
            $user->phone = $this->phone;
        }

        if (!is_null($this->password) && !empty($this->password)) {
            $user->password = $this->setPassword();
        }

	if (!is_null($this->name) && !empty($this->name)) {
            $user->name = $this->name;
        }
		

        if (!is_null($this->firstName) && !empty($this->firstName)) {
	    $user->name = $this->firstName;
            $profile->firstName = $this->firstName;
        }

        if (!is_null($this->lastName) && !empty($this->lastName)) {
            $profile->lastName = $this->lastName;
	    $user->name .= " " . $this->lastName;
        }

        if (!is_null($this->email) && !empty($this->email)) {
            $user->email = $this->email;
        }
        if (!is_null($this->notifications) && !empty($this->notifications)) {
            $user->notifications = $this->notifications;
        }

        if (!is_null($this->deviceToken) && !empty($this->deviceToken)) {
            $userDevice = UserDevice::findOne(['user_id' => $this->user_id]);
            if (!$userDevice) {
                $userDevice = new UserDevice();
            }
            $userDevice->user_id = $user->id;
            $userDevice->device_token = $this->deviceToken;
            $userDevice->save();
        }

        if (!is_null($this->locale) && !empty($this->locale)) {
            $profile->locale = $this->locale;
        }

        if (!is_null($this->gender)) {
            $profile->gender = $this->gender;
        }

        if (!is_null($this->country)) {
            $user->country_id = $this->country;
        }

        if ($user->validate()) {
            $user->save(false);
            $profile->save(false);
            return ArrayHelper::toArray($user, [
                User::className() => [
                    'userID' => function ($user) {
                        return $user->id ?? null;
                    },
                    'firstName' => function ($user) {
                        if (!$user->profile) {
                            return null;
                        }
                        return $user->profile->firstName ?? null;
                    },
                    'lastName' => function ($user) {
                        if (!$user->profile) {
                            return null;
                        }
                        return $user->profile->lastName ?? null;
                    },
                    'phone' => function ($user) {
                        return $user->phone ?? null;
                    },
                    'email' => function ($user) {
                        return $user->email ?? null;
                    },
                    'country' => function ($user) {
                        if ($user->country) {
                            $name = $user->country->NameEn;
                        }
                        return $name ?? null;
                    },
                    'notifications' => function ($user) {
                        return $user->notifications ?? null;
                    },
                    'language' => function ($user) {
                        if (!$user->profile) {
                            return "Not Set";
                        }
                        return $user->profile->locale ?? null;
                    },
                    'gender' => function ($user) {
                        if (!$user->profile) {
                            return "Not Set";
                        }
                        return $user->profile->gender ?? null;
                    },
                ]
            ]);
        } else {
            Yii::$app->response->statusCode = 422;
            $response['error']['code'] = current($user->getFirstErrorCodes()) ?? null;
            $response['error']['message'] = current($user->getFirstErrors()) ?? null;
            return $response;
        }

    }


    public function setPassword()
    {
        return md5($this->password);
    }

    public function validatePhone($attribute)
    {
        $user = User::find()
            ->notDeleted()
            ->andWhere(['phone' => $this->phone])
            ->one();
        if (!empty($user)) {
            $this->addError($attribute, 'This phone has already registered.');
            $this->addErrorCode($attribute, RestErrorCode::ALREADY_REGISTERED_PHONE);
        }
    }

    public function validatePattern($attribute)
    {
        if (!preg_match('/^0[\d]{9}$/', $this->phone)) {
            $this->addError($attribute, 'Phone pattern mismatch.');
            $this->addErrorCode($attribute, RestErrorCode::PHONE_PATTERN_MISMATCH);
        }
    }


    public function validatePasswordLength($attribute)
    {
        if (strlen($this->password) < 6) {
            $this->addError($attribute, 'Password must be at least 6 characters');
            $this->addErrorCode($attribute, RestErrorCode::INCORRECT_PASSWORD_LIMIT);
        }
    }

    public function validateEmailPattern($attribute)
    {
        $user = User::find()
            ->notDeleted()
            ->where(['email' => $this->email])
            ->andWhere(['NOT', ['id' => $this->user_id]])
            ->one();
        if (!empty($user) && $user->isAttributeChanged('email')) {
            $this->addError($attribute, 'This email has been already registered.');
            $this->addErrorCode($attribute, RestErrorCode::ALREADY_REGISTERED_EMAIL);
        }


        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->addError($attribute, 'Invalid Email address');
            $this->addErrorCode($attribute, RestErrorCode::INCORRECT_EMAIL);
        }
    }
}
