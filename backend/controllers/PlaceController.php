<?php

namespace backend\controllers;

use common\models\Category;
use common\models\City;
use common\models\Country;
use common\models\PlaceDetails;
use common\models\PlaceGallery;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use common\models\Place;
use backend\models\search\PlaceSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlaceController implements the CRUD actions for Place model.
 */
class PlaceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'delete',
                'on afterSave' => function ($event){},
            ],

            'delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    /**
     * Lists all Place models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Place model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Place model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Place();
        $modelDetails = new PlaceDetails();
        $modelGallery = new PlaceGallery();
        $countries = ArrayHelper::map(Country::find()->all(), 'id','name');
        $cities = ArrayHelper::map(City::find()->all(), 'id','name');
        $categories = ArrayHelper::map(Category::find()->all(), 'id','name');

        $request = Yii::$app->request;
        $PARAMS = $request->getBodyParams();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $modelDetails->load(Yii::$app->request->post());
            $modelDetails->place_id = $model->id;
            $modelDetails->save();

            $IMAGES =$PARAMS['PlaceGallery']['path'];

            foreach ($IMAGES AS $IMAGE){

                $placeImagesGallery = new PlaceGallery();
                $placeImagesGallery->place_id = $model->id;
                $placeImagesGallery->path = $IMAGE['path'];
                $placeImagesGallery->base_url = $IMAGE['base_url'];

                if($placeImagesGallery->save()){

                }else{
                    print_r($placeImagesGallery->getErrors());die;
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelGallery' => $modelGallery,
            'countries' => $countries,
            'cities' => $cities,
            'categories' => $categories,
        ]);
    }

    /**
     * Updates an existing Place model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $modelDetails = PlaceDetails::find()->where(['place_id' => $model->id])->one();
        $modelGallery = PlaceGallery::find()->where(['place_id' => $model->id])->one();
        $request = Yii::$app->request;
        $PARAMS = $request->getBodyParams();

        if(!$modelDetails){
            $modelDetails = new PlaceDetails();
        }

        if(!$modelGallery){
            $modelGallery = new PlaceGallery();
        }

        $countries = ArrayHelper::map(Country::find()->all(), 'id','name');
        $cities = ArrayHelper::map(City::find()->all(), 'id','name');
        $categories = ArrayHelper::map(Category::find()->all(), 'id','name');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $modelDetails->load(Yii::$app->request->post());
            $modelDetails->place_id = $model->id;
            if($modelDetails->save()){
                $IMAGES =$PARAMS['PlaceGallery']['path'];

                foreach ($IMAGES AS $IMAGE){

                    $placeImagesGallery = new PlaceGallery();
                    $placeImagesGallery->place_id = $model->id;
                    $placeImagesGallery->path = $IMAGE['path'];
                    $placeImagesGallery->base_url = $IMAGE['base_url'];

                    if($placeImagesGallery->save()){
                        continue;
                    }else{
                        print_r($placeImagesGallery->getErrors());die;
                    }
                }
            }else{
                print_r($modelDetails->getErrors());die;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelGallery' => $modelGallery,
            'countries' => $countries,
            'cities' => $cities,
            'categories' => $categories,
        ]);
    }

    /**
     * Deletes an existing Place model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Place model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Place the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Place::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
