<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);
$config = [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
//        'admin' => [
//            'class' => 'backend\modules\admin\Module',
//            'controllerMap' => [
//                'assignment' => [
//                    'class' => 'backend\modules\admin\controllers\AssignmentController',
//                    'userClassName' => 'common\models\User',
//                    'idField' => 'id',
//                    'usernameField' => 'email',
//                    'phoneField' => 'phone',
//                    'fullnameField' => 'name',
//                ],
//                'searchClass' => 'backend\models\search\UserSearch',
//            ],
//            'menus' => [
//                'assignment' => [
//                    'label' => 'Grant Access' // change label
//                ],
//                'route' => null, // disable menu
//            ],
//            'mainLayout' => '@app/views/layouts/main.php',
//
//        ],
    ],
//    'as globalAccess'=>[
//        'class'=>'\common\behaviors\GlobalAccessBehavior',
//        'rules'=>[
//            [
//                'allow' => true,
//                'roles' => ['administrator', 'khilda', 'jubaiha', 'rainbow','zarqa', 'irbid', 'aqaba', 'foodtruck'],
//            ],
//            [
//                'controllers' => ['site'],
//                'allow' => true,
//                'actions' => ['login', 'policy'],
//                'roles' => ['?'],
//            ],
//            [
//                'controllers' => ['site'],
//                'allow' => true,
//                'actions' => ['logout'],
//                'roles' => ['@'],
//            ],
//        ]
//    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => 'WBfoLda3W6rilXEpnQzzT85KHmzDfcRQ',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
//        'api_language' => function(){
//            return strtolower( 'en');
//        },

    ],
    'params' => $params,
];

if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
        'historySize' => 1500
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}


return $config;
