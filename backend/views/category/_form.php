<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'translation')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group col-md-6">
        <?php echo $form->field($model, 'path')->widget(\trntv\filekit\widget\Upload::classname(), [
            'url' => ['upload'],
            'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(jpg|jpe?g|png)$/i'),
            'maxFileSize' => 10 * 2048 * 2048,
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
