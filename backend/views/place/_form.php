<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Place */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 400px; /* The height is 400 pixels */
        width: 100%; /* The width is the width of the web page */
    }
</style>
<div class="place-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <fieldset>
            <legend>Place Details</legend>

            <div class="row col-md-12 margin">
                <div class="form-group col-md-3">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($model, 'translation')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row col-md-12 margin">
                <div class="form-group col-md-3">
                    <?= $form->field($model, 'category_id')->dropDownList($categories) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($model, 'country_id')->dropDownList($countries) ?>
                </div>
            </div>
            <div class="row col-md-12 margin">
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'owner_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'mobile')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'website')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'national_id')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-3">
                    <?= $form->field($modelDetails, 'owner_nid')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group col-md-12">
                    <?= $form->field($modelDetails, 'description')->textarea(['rows' => 7]) ?>
                </div>
            </div>
            <div class="row col-md-12 margin">
                <div class="form-group col-lg-6" style="display: none">
                    <?= $form->field($model, 'latitude')->textInput() ?>
                </div>
                <div class="form-group col-lg-6" style="display: none">
                    <?= $form->field($model, 'longitude')->textInput() ?>
                </div>
                <div class="form-group col-lg-12">
                    <?= $form->field($model, 'address_title')->textInput() ?>
                </div>
                <div id="map"></div>
            </div>
        </fieldset>
    </div>
    <div class="row">
        <fieldset>
            <legend>Images</legend>
            <div class="form-group col-md-12">
                <?php echo $form->field($modelGallery, 'path')->widget(\trntv\filekit\widget\Upload::classname(), [
                    'url' => ['upload'],
                    'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(jpg|jpe?g|png)$/i'),
                    'maxFileSize' => 10 * 2048 * 2048,
                    'maxNumberOfFiles' => 20
                ])->label('Place Images', ['class' => 'pull-left margin']) ?>
            </div>
        </fieldset>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function initMap() {
        var uluru = {lat: 31.994601, lng: 35.829514};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
        google.maps.event.addListener(map, 'click', function (event) {
            var clickedLocation = event.latLng;
            if (marker === false) {
                marker = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true,
                });

                google.maps.event.addListener(marker, 'dragend', function (event) {
                    var currentLocation = marker.getPosition();
                    markerLocation(currentLocation, 'place-latitude', 'place-longitude');
                });
            } else {
                marker.setPosition(clickedLocation);
            }
            var currentLocation = marker.getPosition();
            markerLocation(currentLocation, 'place-latitude', 'place-longitude');
        });
    }

    function markerLocation(currentLocation, lat, lng) {
        getAddress(currentLocation.lat(), currentLocation.lng(), "place-address_title").then(console.log).catch(console.error);
        document.getElementById(lat).value = currentLocation.lat(); //latitude
        document.getElementById(lng).value = currentLocation.lng(); //longitude
    }

    function getAddress(latitude, longitude, div) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            var method = 'GET';
            var url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBUitKIytNkEPSl5mXktJr4k13Xrr4ka30&latlng=' + latitude + ',' + longitude + '&sensor=true';
            var async = true;
            request.open(method, url, async);
            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var data = JSON.parse(request.responseText);
                        var address = data.results[0];
                        console.log(data);
                        document.getElementById(div).value = address['formatted_address'];
                        resolve(address);
                    } else {
                        reject(request.status);
                    }
                }
            };
            request.send();
        });
    };
</script>
<script type="text/javascript" async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsYMCVJPx9KMdkE3MvmzV4taLwMbeOdiA&callback=initMap">
</script>