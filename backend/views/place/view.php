<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Place */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 400px; /* The height is 400 pixels */
        width: 100%; /* The width is the width of the web page */
    }
</style>
<div class="place-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <fieldset>
        <legend>Place details</legend>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'translation',
                'address_title',
                [
                    'attribute' => 'category_id',
                    'value' => function ($data) {
                        return $data->category->name;
                    }
                ],
                [
                    'attribute' => 'country_id',
                    'value' => function ($data) {
                        return $data->country->name;
                    }
                ],
                [
                    'attribute' => 'city_id',
                    'value' => function ($data) {
                        return $data->city->name;
                    }
                ],

            ],
        ]) ?>
        <?php if($model->details): ?>
        <?= DetailView::widget([
            'model' => $model->details,
            'attributes' => [
                'owner_name',
                'mobile',
                'email',
                'website',
                'national_id',
                'owner_nid',
                'description',
            ],
        ]) ?>
        <?php endif; ?>
    </fieldset>
    <fieldset>
        <legend>Map View</legend>
        <div class="row">
            <div id="map"></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Gallery</legend>
        <div class="col-md-8 col-md-push-2">
            <?php
            $gallery = $model->images;
            $items = [];
            foreach ($gallery as $image) {
                $items[] = [
                    'href' => $image->base_url . $image->path,
                    'src' => $image->base_url . $image->path,
                    'poster' => 'http://media.w3.org/2010/05/sintel/poster.png'
                ];
            }
            ?>
            <?= \dosamigos\gallery\Carousel::widget([
                'items' => $items, 'json' => true,
                'clientEvents' => [
                    'onslide' => 'function(index, slide) {
                        console.log(slide);
                    }'
                ]]); ?>
        </div>
    </fieldset>
</div>


<script>
    function initMap() {
        var uluru = {lat: <?= $model->latitude ?>, lng: <?= $model->longitude ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
        google.maps.event.addListener(map, 'click', function (event) {
            var clickedLocation = event.latLng;
            if (marker === false) {
                marker = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true,
                });

            } else {
                marker.setPosition(clickedLocation);
            }
        });
    }
</script>
<script type="text/javascript" async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsYMCVJPx9KMdkE3MvmzV4taLwMbeOdiA&callback=initMap">
</script>